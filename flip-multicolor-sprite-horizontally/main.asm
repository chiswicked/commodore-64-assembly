* = $02 "Zeropage" virtual
ptr:
    .word $0000                 // pointer to sprite bytes
BasicUpstart2(Start)

*=$080e "Variables"
temp:
    .byte $00                   // temp var for misc calc
left:
    .byte $00                   // store joy 2 left state history
right:
    .byte $00                   // store joy 2 right state history

*=* "MAIN PROGRAM"
Start:
    lda #Sprite/$40             // set sprite 0 pointer to our sprite
    sta $07f8
    lda #WHITE                  // set sprite 0 color to white
    sta $d028
    lda #173                    // set sprite 0 x and y pos to 173
    sta $d000
    sta $d001
    lda #%00000001              // switch sprite 0 to multicolor mode
    sta $d01c
    lda #%00000001              // enable sprite 0 
    sta $d015
!loop:
    lda $dc00                   // read joystick port 2
    lsr                         // shift out up
    lsr                         // and down bits
    lsr                         // now we have the left bit
    ror left                    // store it
    lsr                         // now we have the right bit
    ror right                   // store it

    bit left                    // test if joy 2 is left
    bmi !NoFlip+                // not left at all
    bvc !NoFlip+                // was already left at the last joystick readout
    jsr Flip                    // joy 2 just "turned" left -> flip sprite
!NoFlip:
    bit right                   // test if joy 2 is right
    bmi !NoFlip+                // not right at all
    bvc !NoFlip+                // was already right at the last joystick readout
    jsr Flip                    // joy 2 just "turned" right -> flip sprite
!NoFlip:
    jmp !loop-                  // infinite game loop

*=* "FlipByte"
//  reverses bits of a byte by blocks of 2 bits
//  [a,b,c,d,e,f,g,h] becomes [g,h,e,f,c,d,a,b]
//  this is to reverse "multicolor bits"
FlipByte:
    asl                         // rotate 2 bits
    adc  #$80
    rol
    tay                         // save state
    and #%00110011              // save bits that are already
    sta temp                    // in correct position
    tya                         // bring back the original
    and #%11001100              // and mask out bits already in correct position
    
    asl                         // swap uppper and lower nibbles
    adc  #$80                   // to put remaining bits to correct position
    rol
    
    asl                         // by rotating 2 bits twice
    adc  #$80
    rol
    
    clc                         // now combine the two bytes to give
    adc temp                    // the final rotated bit pattern
    rts

.align $40
*=* "Sprite"

Sprite:
.byte $00,$00,$00,$00,$00,$02,$00,$00
.byte $0a,$00,$00,$2a,$00,$01,$68,$00
.byte $05,$58,$00,$17,$d0,$00,$5f,$d0
.byte $01,$7f,$d0,$01,$ff,$d0,$01,$ff
.byte $d0,$05,$ff,$d0,$07,$ff,$d0,$07
.byte $7f,$50,$07,$7f,$40,$07,$7f,$40
.byte $07,$df,$40,$05,$fd,$40,$01,$7d
.byte $00,$00,$55,$00,$00,$00,$00,$82

*=* "Flip"
Flip:
    lda #>Sprite                // load sprite pointer
    sta ptr+1
    lda #<Sprite
    clc
    adc #60                     // but pointing to the 60th byte
    sta ptr                     // 1st byte of the 21st row

    ldx #20                     // we have 21 rows in a sprite
!row:                           // process one row
    ldy #2                      // load the 3rd byte
    lda (ptr),y
    jsr FlipByte                // and flip its bits
    pha                         // store it temporarily
    ldy #0                      // load the 1st byte
    lda (ptr),y
    jsr FlipByte                // and flip its bits
    sta temp                    // store it temporarily
    pla                         // pull back the 3rd flipped byte
    ldy #0                      // and replace the 1st byte with it
    sta (ptr),y 
    lda temp                    // pull back the 1st byte
    ldy #2                      // and replace the 3st byte with it
    sta (ptr),y
    dec ptr                     // move the sprite byte pointer to the 2nd byte
    lda (ptr),y                 // load the 2nd byte
    jsr FlipByte                // and flip its bits
    ldy #2                      // and replace the original 2nd byte with it
    sta (ptr),y
    dec ptr                     // move sprite byte pointer to next row
    dec ptr
    dex                         // decrease row counter
    bpl !row-                   // repeat if there are rows left
rts