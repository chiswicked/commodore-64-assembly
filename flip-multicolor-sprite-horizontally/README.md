# Flip multicolor sprite horizontally

An experiment on horizontally flipping a multicolor sprite by programatically modifying the sprite data.

Move joystick (port 2) left or right to flip the sprite.

The algorithm is probably too expensive (in cycle time) to use in a real project and simply having two different sprite definitions and switching between them is more efficient. Although with plenty of sprites having to have two definitions can be expensive in memory usage.

Is there a way to make this flip programmatically more efficient in cycle time?