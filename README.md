# Commodore 64 Assembly
|                            |    |    |
|----------------------------|----|----|
| Ease-in-out text scrolling | [launch](https://chiswicked.gitlab.io/commodore-64-assembly/ease-in-out-text-scrolling/) | [code](ease-in-out-text-scrolling) |
| Twitch stream intro        | [launch](https://chiswicked.gitlab.io/commodore-64-assembly/twitch-stream-intro/) | [code](twitch-stream-intro) |
| Flip multicolor sprite     | | [code](flip-multicolor-sprite-horizontally) |