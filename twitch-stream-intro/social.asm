//==================================================================================================
//  Social Logos and Links
//==================================================================================================

#importonce
.print "Imported social.asm"

#import "utils.asm"

.const  SOCIAL_TEXT_X       = 10
.var    socialTextY         = 6
.const  SOCIAL_LINE_OFFSET  = 4

.const  SOCIAL_SPRITE_X     = 70
.const  SOCIAL_SPRITE_Y     = 95

// calculate Twtich text positions
.const  TWITCH_TEXT_CHAR_OFFSET     = SOCIAL_TEXT_X + socialTextY * 40
.const  TWITCH_TEXT_SCREEN_RAM      = $0400 + TWITCH_TEXT_CHAR_OFFSET
.const  TWITCH_TEXT_COLOR_RAM       = $d800 + TWITCH_TEXT_CHAR_OFFSET

// calculate Twitter text positions
.eval   socialTextY += SOCIAL_LINE_OFFSET
.const  TWITTER_TEXT_CHAR_OFFSET    = SOCIAL_TEXT_X + socialTextY * 40
.const  TWITTER_TEXT_SCREEN_RAM     = $0400 + TWITTER_TEXT_CHAR_OFFSET
.const  TWITTER_TEXT_COLOR_RAM      = $d800 + TWITTER_TEXT_CHAR_OFFSET

// calculate Gitlab text positions
.eval   socialTextY += SOCIAL_LINE_OFFSET
.const  GITLAB_TEXT_CHAR_OFFSET     = SOCIAL_TEXT_X + socialTextY * 40
.const  GITLAB_TEXT_SCREEN_RAM      = $0400 + GITLAB_TEXT_CHAR_OFFSET
.const  GITLAB_TEXT_COLOR_RAM       = $d800 + GITLAB_TEXT_CHAR_OFFSET

// calculate Discord text positions
.eval   socialTextY += SOCIAL_LINE_OFFSET
.const  DISCORD_TEXT_CHAR_OFFSET    = SOCIAL_TEXT_X + socialTextY * 40
.const  DISCORD_TEXT_SCREEN_RAM     = $0400 + DISCORD_TEXT_CHAR_OFFSET
.const  DISCORD_TEXT_COLOR_RAM      = $d800 + DISCORD_TEXT_CHAR_OFFSET

.const  STATE_ANIM_LENGTH           = 80    // state transition after 80 frames
.const  STATE_ANIM_CYCLE            = 40    // one cycle is 40 color shifts, same as number of chars per row
.const  NO_OF_STATES                = 4     // bidirectional, left, bidirectional, right

Social: {
    Init: {
        lda #NO_OF_STATES-1
        sta state
        lda #STATE_ANIM_LENGTH-1
        sta counter
        lda #0                              // left cycle starts from 0
        sta leftCycle                       // and goes up to STATE_ANIM_CYCLE-1
        lda #STATE_ANIM_CYCLE-1             // right cycle starts from STATE_ANIM_CYCLE-1
        sta rightCycle                      // and goes down to 0

        //  load and store social text into screen RAM
        ldx #21                             // all social text are fixed 22 char length
    !loop:
        lda TwitchTextData,x
        sta TWITCH_TEXT_SCREEN_RAM,x
        lda TwitterTextData,x
        sta TWITTER_TEXT_SCREEN_RAM,x
        lda GitlabTextData,x
        sta GITLAB_TEXT_SCREEN_RAM,x
        lda DiscordTextData,x
        sta DISCORD_TEXT_SCREEN_RAM,x
        dex
        bpl !loop-
        rts
    }
    TwitchTextData:
        .encoding "screencode_mixed"
        .text "twitch.com/chiswicked "
    TwitterTextData:
        .encoding "screencode_mixed"
        .text "twitter.com/chiswicked"
    GitlabTextData:
        .encoding "screencode_mixed"
        .text "gitlab.com/chiswicked "
    DiscordTextData:
        .encoding "screencode_mixed"
        .text "discord.gg/ppqG7Ps    "

    Draw: {
        VICBank(0);
        lda #%00011011                      // switch to text mode and move text up a notch
        sta $d011
        lda #%11100000                      // set hi-res text mode
        sta $d016
        lda #%00010110                      // set pointer to char memory and use mixed upper and lower case letters
        sta $d018
        rts
    }

    DrawSprites: {
        ldx #6
    !loop:
        lda SocialSpritePtr, x              // load current sprite pointer value and
        sta $07f8, x                        // set it in corresponding sprite pointer memory
        lda SocialSpriteColor, x            // load current sprite color value and
        sta $d027, x                        // set it in corresponding sprite color memory
        dex
        bpl !loop-
        
        lda #SOCIAL_SPRITE_X
        sta $d000                           // sprite0 X pos (twitch1)
        sta $d002                           // sprite1 X pos (twitch2)
        sta $d004                           // sprite2 X pos (twitter)
        sta $d006                           // sprite3 X pos (gitlab1)
        sta $d008                           // sprite4 X pos (gitlab2)
        sta $d00a                           // sprite5 X pos (gitlab3)
        sta $d00c                           // sprite6 X pos (discord)
        
        lda #SOCIAL_SPRITE_Y
        sta $d001                           // sprite0 X pos (twitch1)
        sta $d003                           // sprite1 X pos (twitch2)
        lda #SOCIAL_SPRITE_Y+29
        sta $d005                           // sprite2 X pos (twitter)
        lda #SOCIAL_SPRITE_Y+60
        sta $d007                           // sprite3 X pos (gitlab1)
        sta $d009                           // sprite4 X pos (gitlab2)
        sta $d00b                           // sprite5 X pos (gitlab3)
        lda #SOCIAL_SPRITE_Y+93
        sta $d00d                           // sprite6 X pos (discord)

        lda #%00000000                      // switch sprites to hi-res mode
        sta $d01c

        lda #%01111111                      // enable sprites 0-6 
        sta $d015
        rts
    }
    SocialSpriteColor:
        // twtich1, twitch2, twitter, gitlab1, gitlab2, gitlab3, discord
        .byte WHITE, PURPLE, CYAN, RED, ORANGE, LIGHT_RED, LIGHT_BLUE
    SocialSpritePtr:
        // twtich1, twitch2, twitter, gitlab1, gitlab2, gitlab3, discord
        .byte 200, 201, 202, 203, 204, 205, 206
    counter:
        .byte $00
    state:
        .byte $00
    leftCycle:
        .byte $00
    rightCycle:
        .byte $00
    temp1:
        .byte $00
    temp2:
        .byte $00

    ColorCycle: {
        lda #1                              // check bit0
        bit state                           // if it's set (state 1 and 3)
        bne !bidir_scroll+                  // cycle colors bidirectionally

        lda #2                              // check bit1
        bit state                           // if it's set
        bne !left_scroll+                   // cycle colors left (state 2)
        jmp !right_scroll+                  // clear cycle colors right (state 4)

    // state %01 and 11 cycle colors bidirectionally
    !bidir_scroll:
        ldx #STATE_ANIM_CYCLE-1             // use x to iterate through one row
        txa                                 // calculate color cycle left position
        clc                                 // based on char position (x)
        adc leftCycle                       // offset by left cycle iteration
        sta temp1                           // and store it in temp var

        txa                                 // calculate color cycle right position
        clc                                 // based on char position (x)
        adc rightCycle                      // offset by right cycle iteration
        sta temp2                           // and store it in temp var

    !loop:
        // cycle colors left for twitch and gitlab
        ldy temp1
        lda colorData,y
        sta TWITCH_TEXT_COLOR_RAM,x
        lda colorData,y
        sta GITLAB_TEXT_COLOR_RAM,x
        dec temp1

        // cycle colors right for twitter and discord
        ldy temp2
        lda colorData,y
        sta TWITTER_TEXT_COLOR_RAM,x
        lda colorData,y
        sta DISCORD_TEXT_COLOR_RAM,x
        dec temp2
        dex
        bpl !loop-

        //  iterate left cycles
        inc leftCycle                       // next cycle iteration
        lda leftCycle
        cmp #STATE_ANIM_CYCLE               // if reaches end
        bne !+
        lda #0                              // reset it to start value
        sta leftCycle
!:
        //  iterate right cycles
        dec rightCycle                      // next cycle iteration
        lda rightCycle
        bpl !+                              // if goes below 0
        lda #STATE_ANIM_CYCLE-1             // reset it to start value
        sta rightCycle
!:

        jmp !done+                          // done with animations

    //  state %00 cycle colors left
    !left_scroll:
        ldx #STATE_ANIM_CYCLE-1             // use x to iterate through one row
        txa                                 // calculate cycle position
        clc                                 // based on char position (x)
        adc leftCycle                       // offset by left cycle iteration
        tay                                 // and store it in y for absolute indexing
    !loop:
        lda colorData,y
        sta TWITCH_TEXT_COLOR_RAM,x       // put it into Color Ram into column x
        lda colorData,y
        sta GITLAB_TEXT_COLOR_RAM,x       // put it into Color Ram into column x
        lda colorData,y
        sta TWITTER_TEXT_COLOR_RAM,x      // ... and write it to Color Ram
        lda colorData,y
        sta DISCORD_TEXT_COLOR_RAM,x      // ... and write it to Color Ram
        dey
        dex                                 // decrement x-register to go to next iteration
        bpl !loop-                          // repeat if there are iterations left

        //  iterate left cycles
        inc leftCycle                       // next cycle iteration
        lda leftCycle
        cmp #STATE_ANIM_CYCLE               // if reaches end
        bne !+
        lda #0                              // reset it to start value
        sta leftCycle
!:
        jmp !done+                          // done with animations

    //  state %10 cycle colors right
    !right_scroll:
        ldx #STATE_ANIM_CYCLE-1             // use x to iterate through one row
        txa                                 // calculate cycle position
        clc                                 // based on char position (x)
        adc rightCycle                      // offset by left cycle iteration
        tay                                 // and store it in y for absolute indexing

    //  load next color data and store it in corresponding color RAM
    !loop:
        lda colorData,y
        sta TWITCH_TEXT_COLOR_RAM,x
        lda colorData,y
        sta GITLAB_TEXT_COLOR_RAM,x
        lda colorData,y
        sta TWITTER_TEXT_COLOR_RAM,x
        lda colorData,y
        sta DISCORD_TEXT_COLOR_RAM,x
        dey
        dex
        bpl !loop-

        //  iterate right cycles
        dec rightCycle                      // next cycle iteration
        lda rightCycle
        bpl !+                              // if goes below 0
        lda #STATE_ANIM_CYCLE-1             // reset it to start value
        sta rightCycle
    !:

    //  finished current state animation
    !done:
        dec counter                         // count down
        lda counter
        bpl !+                              // and if reached end of animation
        lda #STATE_ANIM_LENGTH-1            // reset counter
        sta counter
        dec state                           // and switch to next animation state
        bpl !+                              // if goes blow 0
        lda #NO_OF_STATES-1                 // rest to start value
        sta state
    !:
        rts
    }
    colorData:
        .byte $07,$07,$01,$01,$01           // YELLOW,YELLOW,WHITE,WHITE,WHITE
        .byte $01,$01,$01,$01,$01           // WHITE,WHITE,WHITE,WHITE,WHITE
        .byte $01,$01,$01,$01,$01           // WHITE,WHITE,WHITE,WHITE,WHITE
        .byte $01,$01,$01,$07,$07           // WHITE,WHITE,WHITE,YELLOW,YELLOW
        .byte $0f,$0f,$0a,$0a,$08           // LIGHT_GRAY,LIGHT_GRAY,LIGHT_RED,LIGHT_RED,ORANGE
        .byte $08,$02,$02,$09,$09           // ORANGE,RED,RED,BROWN,BROWN
        .byte $09,$09,$02,$02,$08           // BROWN,BROWN,RED,RED,ORANGE
        .byte $08,$0a,$0a,$0f,$0f           // ORANGE,LIGHT_RED,LIGHT_RED,LIGHT_GRAY,LIGHT_GRAY
        .byte $07,$07,$01,$01,$01           // YELLOW,YELLOW,WHITE,WHITE,WHITE
        .byte $01,$01,$01,$01,$01           // WHITE,WHITE,WHITE,WHITE,WHITE
        .byte $01,$01,$01,$01,$01           // WHITE,WHITE,WHITE,WHITE,WHITE
        .byte $01,$01,$01,$07,$07           // WHITE,WHITE,WHITE,YELLOW,YELLOW
        .byte $0f,$0f,$0a,$0a,$08           // LIGHT_GRAY,LIGHT_GRAY,LIGHT_RED,LIGHT_RED,ORANGE
        .byte $08,$02,$02,$09,$09           // ORANGE,RED,RED,BROWN,BROWN
        .byte $09,$09,$02,$02,$08           // BROWN,BROWN,RED,RED,ORANGE
        .byte $08,$0a,$0a,$0f,$0f           // ORANGE,LIGHT_RED,LIGHT_RED,LIGHT_GRAY,LIGHT_GRAY
}
