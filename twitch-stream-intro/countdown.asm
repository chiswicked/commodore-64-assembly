//==================================================================================================
//  Countdown Clock and Text
//==================================================================================================

#importonce
.print "Imported irq.asm"

#import "zeropage.asm"

.const COUNTDOWN_MINUTE             = $16   // start counting down from 16 mins
.const COUNTDOWN_SECOND             = $00   // 0 seconds
.const COUNTDOWN_DIGITS_X           = 135   // digit sprites to start from x pos
.const COUNTDOWN_DIGITS_Y           = 257   // digit sprites to start from y pos
.const COUNTDOWN_TEXT               = "STREAM STARTING IN"
.const COUNTDOWN_TEXT_Y             = 24    // countdown text char x pos

// where to store countdown text in screen and color RAM to display on screen vertically centered
.const COUNTDOWN_TEXT_SCREEN_RAM    = $0400+20-floor((COUNTDOWN_TEXT.size())/2)+COUNTDOWN_TEXT_Y*40
.const COUNTDOWN_TEXT_COLOR_RAM     = $d800+20-floor((COUNTDOWN_TEXT.size())/2)+COUNTDOWN_TEXT_Y*40

Clock: {
    Init: {
        lda #COUNTDOWN_MINUTE               // initialize BCD minute value
        sta clockMinute
        lda #COUNTDOWN_SECOND               // initialize BCD second value
        sta clockSecond
        lda #217                            // initialize min - sec divider sprite pointer
        sta minSecDivider

    //  load countdown text into screen and color RAM
        ldx #COUNTDOWN_TEXT.size()-1
    !loop:
        lda TextStreamPtr,x
        sta COUNTDOWN_TEXT_SCREEN_RAM,x
        lda #BLACK
        sta COUNTDOWN_TEXT_COLOR_RAM,x
        dex
        bpl !loop-
        rts
    }

    TextStreamPtr:
        .encoding "screencode_mixed"
        .text COUNTDOWN_TEXT

    Countdown: {
        dec frameCount                      // decrement frame counter
        bne !skip+                          // if still > 0 don't change clock digits

        lda #FRAMERATE                      // reset frame counter
        sta frameCount
    
        //  count down one second
    DecTimerSec:
        sed                                 // calculate in binary coded decimal (BCD)
        lda clockSecond                     // load current minute...
        sec                                 //
        sbc #1                              // ...and subtract one
        sta clockSecond                     // store result back in zeropage var
        cld                                 // turn off BCD
        bpl !skip+                          //

        // count down one minute
    DecTimerMin:
        sed                                 // calculate in binary coded decimal (BCD)
        lda clockMinute                     // load current minute...
        sec                                 // 
        sbc #1                              // ...and subtract one
        sta clockMinute                     // store result back in zeropage var
        cld                                 // turn off BCD
        bpl ResetTimerSec                   // haven't reached 0 min 0 sec yet
        lda #0                              // it's 0 min 0 sec, stop countdown
        sta clockMinute                     // set min to 0
        sta clockSecond                     // set sec to 0
        jmp !skip+                          // done with countdown
    ResetTimerSec:
        lda #$59                            // "new" minute startring from 59 seconds
        sta clockSecond
    !skip:
        jsr updatePtrs                      // update sprite pointers based on current time
        rts
    }

    DrawDigits: {
        ldx #5
    !loop:
        lda min10Ptr, x                     // set sprite pointers for digits
        sta $07f8, x
        lda #BLACK                          // set all sprites to black
        sta $d027, x
        dex
        bpl !loop-

        //  set countdown digits x pos
        lda #COUNTDOWN_DIGITS_X+00          // min 10 digit x pos...
        sta $d000                           // ...to sprite 0 x pos
        lda #COUNTDOWN_DIGITS_X+17          // min 01 digit x pos...
        sta $d002                           // ...to sprite 1 x pos
        lda #COUNTDOWN_DIGITS_X+43          // sec 10 digit x pos...
        sta $d004                           // ...to sprite 2 x pos
        lda #COUNTDOWN_DIGITS_X+60          // sec 01 digit x pos...
        sta $d006                           // ...to sprite 3 x pos
        lda #COUNTDOWN_DIGITS_X+30          // min - sec divider (:) x pos...
        sta $d008                           // ...to sprite 4 y pos

        //  set countdown digits y pos
        lda #COUNTDOWN_DIGITS_Y
        sta $d001                           // sprite 0: min 10 digit y pos
        sta $d003                           // sprite 1: min 01 digit y pos
        sta $d005                           // sprite 2: sec 10 digit y pos
        sta $d007                           // sprite 3: sec 01 digit y pos
        sta $d009                           // sprite 4: min - sec divider (:) y pos

        lda #%00000000                      // set sprites to hi-res
        sta $d01c

        lda #%00011111                      // enable sprites 0-4
        sta $d015
        rts
    }

    //  update countdown digits sprite pointers based on current time
    updatePtrs: {
        lda clockMinute
        jsr TensDigitOffset
        sta min10Ptr
        lda clockMinute
        jsr OnesDigitOffset
        sta min01Ptr
        lda clockSecond
        jsr TensDigitOffset
        sta sec10Ptr
        lda clockSecond
        jsr OnesDigitOffset
        sta sec01Ptr
        rts
    }

    DigitsSpritePtr:
    // digits 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
    .byte 207, 208, 209, 210, 211, 212, 213, 214, 215, 216

//==================================================================================================
//  TensDigitOffset(A) A
//==================================================================================================
//  Returns the offset for the 10s decimal place digit sprite from lookup table
//--[Input]-----------------------------------------------------------------------------------------
//  A = BCD value of a 2 digit number
//--[Output]----------------------------------------------------------------------------------------
//  A = offset from Digits address
//==================================================================================================
    TensDigitOffset: {
        and #$F0                    // mask out ones value 
        lsr                         // move upper nibble to lower nibble
        lsr                         // (move tens value to ones value position)
        lsr                         // by shifting bits right 4 times
        lsr                         // 
        tax                         // move ones value to X for absolute lookup
        lda DigitsSpritePtr,x       // look up offset value
        rts                         // return
    }

//==================================================================================================
//  OnesDigitOffset(A) A
//==================================================================================================
//  Returns the offset for the 01s decimal place digit sprite from lookup table
//-[Input]------------------------------------------------------------------------------------------
//  A = BCD value of a 2 digit number
//-[Output]-----------------------------------------------------------------------------------------
//  A = offset from Digits address
//==================================================================================================
    OnesDigitOffset: {
        and #$0F                    // mask out tens value
        tax                         // move ones value to X for absolute lookup
        lda DigitsSpritePtr,x       // look up offset value
        rts                         // return
    }
}
