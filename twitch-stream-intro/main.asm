//==================================================================================================
//  Main Code
//==================================================================================================

#import "zeropage.asm"
#import "utils.asm"

.const  FRAMERATE = 50                      // for lazy, somewhat inaccurate time measurement in PAL

BasicUpstart2(Start)

.var LogoBitmap             = LoadBinary("assets/chiswickedlogo.kla", BF_KOALA)
.var ScrollTextCharMapFile  = LoadBinary("assets/scrollfont.bin")

//==================================================================================================
//  MAIN PROGRAM LOOP
//==================================================================================================

* = $2000 "Main Program"
#import "irq.asm"

Start:
    sei                                     // disable interrups
    lda #FRAMERATE-1                        // initialize frame counter
    sta frameCount
    ClearCharScreen(BLACK)                  // clear screen
    jsr Logo.Init                           // initialize logo colors
    jsr Social.Init                         // load social text into screen RAM
    jsr ScrollText.Init                     // load scroll text into screen RAM
    jsr Clock.Init                          // initialize static countdown text
    jsr IRQ.Init                            // initialize interrupts, home of the main program
    jmp *                                   // infinite program loop

//==================================================================================================
//  GRAPHICS
//==================================================================================================

* = $3200 "Sprites"
#import "assets/social.asm"
#import "assets/digits.asm"

* = $4c00 "Screen RAM VIC Bank 1"
    .fill LogoBitmap.getScreenRamSize(), LogoBitmap.getScreenRam(i)

* = $6000 "Logo Bitmap"
LogoBitmapData:
    .fill $a00, LogoBitmap.getBitmap(i)

* = $7f40 "Logo Color RAM"
LogoColorRAMData:
    .fill LogoBitmap.getColorRamSize(), LogoBitmap.getColorRam(i)

* = $c800 "Scroll Text Character Map"
ScrollTextCarMap:
    .fill ScrollTextCharMapFile.getSize(), ScrollTextCharMapFile.get(i)

//==================================================================================================
//  DEBUG INFO
//==================================================================================================

.print ""
.print "Logo Graphics Data"
.print "------------------"
.print "Koala format:   "+BF_KOALA
.print "ScreenRAM size: "+LogoBitmap.getScreenRamSize()
.print "ColorRAM size:  "+LogoBitmap.getColorRamSize()
.print "Bitmap size:    "+LogoBitmap.getBitmapSize()
