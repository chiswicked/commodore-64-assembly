//==================================================================================================
//  General Utilities, Macros
//==================================================================================================

#importonce
.print "Imported utils.asm"

//  SetBorderColor(color)
.macro SetBorderColor(color) {
    lda #color
    sta $d020
}

//  SetBackgroundColor(color)
.macro SetBackgroundColor(color) {
    lda #color
    sta $d021
}
//  SetScreenColor(color)
//  set both border and background color
.macro SetScreenColor(color) {
    lda #color
    sta $d020
    sta $d021
}

//  ClearCharScreen(color)
//  fill screen with spaces and set foreground color
.macro ClearCharScreen(color) {
    ldx #0
!loop:
    lda #$20                    // #$20 = spacebar
    sta $0400,x                 // fill screen RAM with spacebar characters
    sta $0400+$100,x 
    sta $0400+$200,x 
    sta $0400+$300,x 
    lda color                   // set foreground color
    sta $d800,x                 // fill color RAM with given color
    sta $d800+$100,x 
    sta $d800+$200,x 
    sta $d800+$300,x 
    inx
    bne !loop-
}

//  VICBank(bank)
//  set VIC Bank (0-3) to use for graphics
//  see https://www.c64-wiki.com/wiki/VIC_bank
.macro VICBank(bank) {
    .if ((bank<0) || (bank>3)) .error "VIC Bank must be 0-3, "+bank+" is not a valid value"
    .var pattern = 3 - bank
    lda $dd00
    and #%11111100
    ora #pattern
    sta $dd00
}

//  SetColorRAM(colormap)
//  copy colormap into color RAM
//  see https://www.c64-wiki.com/wiki/Color_RAM
.macro SetColorRAM(colormap) {
    ldx #0
!loop:
    lda colormap+$000,x
    sta $d800   +$000,x
    lda colormap+$100,x
    sta $d800   +$100,x
    lda colormap+$200,x
    sta $d800   +$200,x
    lda colormap+$300,x
    sta $d800   +$300,x
    inx
    bne !loop-
}

//  ResetCharScrollX(ptr_to_x)
//  sets horizontal raster scroll to value at ptr_to_x address
.macro SetCharScrollX(ptr_to_x) {
    lda $d016
    and %11111000
    ora ptr_to_x
    ora #%00001000
    sta $d016
}

//  ResetCharScrollX()
//  sets horizontal raster scroll to 0
.macro ResetCharScrollX() {
    lda $d016
    and #%11111000
    sta $d016
}

//  SetMultiColor38ColCharMode()
//  sets horizontal raster scroll to 0
.macro SetMultiColor38ColCharMode() {
    lda #%11110000
    sta $d016
}

//  SetHiRes38ColCharMode()
//  sets horizontal raster scroll to 0
.macro SetHiRes38ColCharMode() {
    lda #%11100000
    sta $d016
}

//  SetMultiColor40ColCharMode()
//  sets horizontal raster scroll to 0
.macro SetMultiColor40ColCharMode() {
    lda #%11111000
    sta $d016
}

//  SetHiRes40ColCharMode()
//  sets horizontal raster scroll to 0
.macro SetHiRes40ColCharMode() {
    lda #%11101000
    sta $d016
}
