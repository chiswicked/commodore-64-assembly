//==================================================================================================
//  Chiswicked Commodore 64 Logo Graphics
//==================================================================================================

#importonce
.print "Imported logo.asm"

#import "utils.asm"

Logo: {
    Init: {
        SetColorRAM(LogoColorRAMData)
        rts
    }
    Draw: {
        VICBank(1);
    //  technically this is not required as the background color
    //  of the top and bottom part of the screen are the same and
    //  it's enough to set once on the bottom part
        SetScreenColor(LogoBitmap.getBackgroundColor())
        lda #%00111011                      // switch to graphics mode
        sta $d011
        lda #%11011000                      // set multicolor graphics mode 
        sta $d016
        lda #%00111000
        sta $d018                           // set pointer to graphics memory 
        rts
    }
}
