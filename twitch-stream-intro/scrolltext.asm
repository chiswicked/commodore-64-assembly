//==================================================================================================
//  Text Scroller (shoutouts)
//==================================================================================================

#importonce
.print "Imported scrolltext.asm"

.const SCROLL_TEXT_ANIM_LENGTH      = 30    // length of scroll animation in number of frames 
.const SCROLL_TEXT_NO_ANIM_LENGTH   = 6     // how long to show scrolled in text (multiples of anim length)
.const EASE_IN_FROM                 = 319   // from right side of screen
.const EASE_IN_TO                   = 0     // to left side of screen

.var textList = List().add(
    "                                        ",
    "  Welcome to chiswicked's C64 stream!   ",
    "    Intro graphics were made using:     ",
    "          https://mcdraw.xyz/           ",
    "      https://www.spritemate.com/       ",
    "  Thanks for the inspiration and help:  ",
    " andymagicknight furroy shallan stepz   ",
    "  chromosundrift hayesmaker zooperdan   ",
    "   awsm deadline oldskoolcoder proton   ",
    " and the whole C64 Twitch community...  ",
    "   Let's keep the Commodore 64 alive!   "
)

.eval textList.add(textList.get(0))         // copy first line to last for smooth transition to restart

.const TEXT_LIST_LENGTH = textList.size()

ScrollText: {
    Init: {
        lda #<Text                          // load and store a pointer (lower byte)
        sta scrollTextPtr                   // to the first byte of the scrolling text
        lda #>Text                          // load and store a pointer (upper byte)
        sta scrollTextPtr+1                 // to the first byte of the scrolling text
        lda #0                              // start counting which item in the list we're scrolling
        sta offsetCounter 
        rts
    }
    animTimer:
        .byte $00                           // frame counter to measure length of a state
    state:
        .byte $00                           // 0: we're scrolling, otherwise show static text
    charScrollX: 
        .byte $00                           // temp storage for next animation frame's char scroll x

    //  increment animation counters and manage state
    Tick: {
        inc animTimer                       // move onto next animation frame
        lda animTimer
        cmp #SCROLL_TEXT_ANIM_LENGTH        // if we reached the end of animation cycle
        bne !done+                          // no: nothing else to do
        lda #0                              // yes: reset animation timer to initial value
        sta animTimer
        
        inc state                           // and move onto next state
        lda state                           
        cmp #1                              // if current state is 1
        bne !didntstop+                     // no: we're either animating or showing static text
                                            // yes: we just finished the scroll animation and should stop

        //  state transition: finished animation, onto next item in scroll text list
        lda scrollTextPtr                   // and move the pointer to the next line of text to scroll
        clc
        adc #40                             // which is always offset by 40 bytes 
        sta scrollTextPtr                   // so add 40 to the pointer lower byte
        bcc !nocarry+                       // and if the carry was set
        inc scrollTextPtr+1                 // increase the upper byte by one
    !nocarry:
        jmp !done+                          // nothing else to do
    !didntstop:
        lda state                           // if reached the end of states
        cmp #SCROLL_TEXT_NO_ANIM_LENGTH+1   // 
        bne !done+                          // no: nothing else to do, we continue showing static text
                                            // yes: we start new animation

        // state transition: finished showing static text, onto next animation
        lda #0                              // set state to 0 (animating)
        sta state
        inc offsetCounter                   // count which item we're animating
        lda offsetCounter                   // if we just scrolled the last
        cmp #TEXT_LIST_LENGTH-1             // item in the scroll text list
        bne !done+                          // no: nothing else to do
        jsr Init                            // yes: reset everything to initial values
    !done:
        rts
    }
    offsetCounter:
        .byte $00                           // line of text we're scrolling
    Draw: {
        //  set y depending on state
        //  static text    y = 0
        //  scrolling text y = current animation frame
        ldy state                           // if 0
        beq !anim+                          // yes: we are animating
        ldy #0                              // no: show static text, always from offset 0
        jmp !skipanim+
    !anim:
        ldy animTimer                       // if we're animating load the current animation frame
    !skipanim:

        //  start drawing text from the position dictated by the y register
        lda TextShiftPos,y                  // load currenct char scroll x
        sta charScrollX                     // and store it for later use in a different interrupt

        //  display text from current TextCharPosX
        lda TextCharPos,y                   // load position of text for current animation frame
        tay                                 // to use for indexing screen RAM

        //  put individual chars into corresponding screen RAM
        ldx #0
    !loop:
        lda (scrollTextPtr),y               // load next char of scrolling text
        sta $0400+21*40,x                   // and store it in relevant screen RAM position
        iny                                 // move onto next char of scrolling text
        inx                                 // move onto next screen RAM position
        cpx #40
        bne !loop-                          // loop through all 40 characters of scrolling text
        rts
    }

    Shift: {
        //  set char scroll x based on the value we precalculated in a previous raster interrupt
        SetCharScrollX(charScrollX)
        rts
    }

    Text:
    //  generate scroll text lookup table
        .for (var i=0;i<TEXT_LIST_LENGTH;i++) .text textList.get(i)
    TextCharPos:
    //  generate lookup table for char pos of scrolling text for each animation frame
        .fill SCROLL_TEXT_ANIM_LENGTH, floor(easeIn(i)/8)
    TextShiftPos:
    //  generate lookup table for char scroll x pos of scrolling text for each animation frame
        .fill SCROLL_TEXT_ANIM_LENGTH, 7-mod(easeIn(i),8)

    //  Debug info: scrolling text positions for each animation frame
        .for(var i=0;i<SCROLL_TEXT_ANIM_LENGTH;i++)
            .print "Frame " + toIntString(i,2) + ".   pixel x pos: " + toIntString(easeIn(i),3) + "   char pos: " + toIntString(floor(easeIn(i)/8),2) + "   char scroll x: " + (7-mod(easeIn(i),8))
}

//  generate ease-in-out text scroll x pos for given animation frame
.function easeIn(i) {
    .return round(EASE_IN_FROM/2-(EASE_IN_FROM-EASE_IN_TO)/2*(cos(toRadians(180 * i / SCROLL_TEXT_ANIM_LENGTH))))
}


