//==================================================================================================
//  Zeropage Memory Mapping
//==================================================================================================

#importonce

* = $02 "Zeropage" virtual

frameCount:
    .byte $00                   // frame counter to lazily measure seconds
clockMinute:
    .byte $00                   // countdown minutes in BCD
clockSecond:
    .byte $00                   // countdown seconds in BCD
min10Ptr:
    .byte $00                   // sprite pointer to min 10 digit
min01Ptr:
    .byte $00                   // sprite pointer to min 01 digit
sec10Ptr:
    .byte $00                   // sprite pointer to sec 10 digit
sec01Ptr:
    .byte $00                   // sprite pointer to sec 01 digit
minSecDivider:
    .byte $00                   // sprite pointer to min - sec divider (:)
scrollTextPtr:
    .word $0000                 // pointer to where we're at in out scrolling text