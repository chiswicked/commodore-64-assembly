//==================================================================================================
//  Interrupt Handling to Draw Different Screen Elements
//==================================================================================================

#importonce
.print "Imported irq.asm"

#import "logo.asm"
#import "social.asm"
#import "scrolltext.asm"
#import "countdown.asm"

//  Screen split organised in files as:

//======================================//
//                                      //
//                logo.asm              //
//======================================//
//                                      //
//                                      //
//              social.asm              //
//======================================//
//          scrolltext.asm              //
//======================================//
//                                      //
//           countdown.asm              //
//======================================//

IRQ: {
    .const IRQ0_LINE = 0                    // vblank / start of logo section
    .const IRQ1_LINE = 86                   // start of social section
    .const IRQ2_LINE = 212                  // start of scroll text section
    .const IRQ3_LINE = 235                  // start of countdown section
    .const IRQ4_LINE = $fa                  // raster line to trigger opening bottom border

    Init: {
        ldy #$7f                            // $7f = %01111111
        sty $dc0d                           // turn off CIAs Timer interrupts
        sty $dd0d                           // turn off CIAs Timer interrupts
        lda $dc0d                           // cancel all CIA-IRQs in queue/unprocessed
        lda $dd0d                           // cancel all CIA-IRQs in queue/unprocessed
        lda #$01                            // set IRQ mask...
        sta $d01a                           // ...to trigger by raster line
        SetNextInterrupt(IRQ0_LINE, handle0)
        cli                                 // clear interrupt disable flag
        rts
    }

    handle0: {
        StoreState()
        ResetCharScrollX()
        jsr Clock.Countdown                 // decrease frame counter and adjust clock digits
        jsr Logo.Draw                       // switch to multicolor graphics mode and draw logo
        jsr Social.DrawSprites              // preload social sprites
        jsr Social.ColorCycle               // iterate color cycling
        jsr ScrollText.Tick                 // iterate scroll text animation frames
        jsr ScrollText.Draw                 // preload scroll text into screen RAM
        SetNextInterrupt(IRQ1_LINE, handle1)
        RestoreState()
    }

    handle1: {
        StoreState()
        nop                                 // lazy raster line stabilization
        nop
        nop
        SetScreenColor(BLACK)
        jsr Social.Draw
        SetNextInterrupt(IRQ2_LINE, handle2)
        RestoreState()
    }

    handle2: {
        StoreState()
        SetHiRes38ColCharMode()
        SetScreenColor(LIGHT_GRAY)
        jsr ScrollText.Shift
        SetNextInterrupt(IRQ3_LINE, handle3)
        RestoreState()
    }

    handle3: {
        StoreState()
        SetHiRes40ColCharMode()
        ResetCharScrollX()
        jsr Clock.DrawDigits
        SetNextInterrupt(IRQ4_LINE, handle4)
        RestoreState()
    }

    handle4: {
        StoreState()
        lda #$00                            // switch screen off in that position
        sta $d011                           // to open up bottom border for sprites
        SetNextInterrupt(IRQ0_LINE, handle0)
        RestoreState()
    }
}

//  SetNextInterrupt(rasterLine, irqHandler)
//  trigger next interrupt handler (irqHandler) at raster line (rasterLine)
//  for simplicity no support for raster lines > 255
.macro SetNextInterrupt(rasterLine, irqHandler) {
    lda #rasterLine                         // trigger next interrupt at #rasterLine
    sta $d012                               // raster line to generate next interrupt

    lda $d011                               // set bit #0 to 0 as we don't need to support
    and #$7f                                // positions > 255
    sta $d011

    lda #<irqHandler                        // point IRQ vector to our custom irq routine...
    sta $0314                               // ...lower nibble
    lda #>irqHandler
    sta $0315                               // ...upper nibble
}

//  StoreState()
//  push a, x, y registers onto stack
.macro StoreState() {
    pha
    txa 
    pha 
    tya 
    pha
}

//  RestoreState()
//  pull y, x, a registers from stack
.macro RestoreState() {
    asl $d019                               // acknowledge IRQ
    pla
    tay
    pla
    tax 
    pla
    jmp $ea81                               // return to kernel interrupt routine
}
