.const SCROLL_TEXT          = " chiswicked "    // text to scroll
.const SCROLL_LENGTH        = 100               // number of animation frames
.const SCROLL_CHAR_ROW_POS  = 12                // row to scroll the text on

// raster lines to trigger interrupts at
.const IRQ0_LINE            = 0
.const IRQ1_LINE            = 100
.const IRQ2_LINE            = SCROLL_CHAR_ROW_POS * 8 + 17
.const IRQ3_LINE            = IRQ2_LINE + 73

//--------------------------------------------------------------------------------------------------
//  Zero page definition
//--------------------------------------------------------------------------------------------------

* = $02 "Zeropage" virtual

frameCount:
    .byte $00
temp:
    .byte $00

//--------------------------------------------------------------------------------------------------
//  Main program
//--------------------------------------------------------------------------------------------------

BasicUpstart2(Start)

*=$2400
Start:
    sei                         // set interrupt disable flag

//  init animation length
    lda #SCROLL_LENGTH
    sta frameCount

//  clear screen and set background color
    ldx #0
InitScreen:
    lda #$20                    // spacebar character
    sta $0400+$000,x            // fill four areas with 256 spacebar characters
    sta $0400+$100,x            // to clear the entire screen
    sta $0400+$200,x            //
    sta $0400+$300,x            //
    lda #BLACK                  // set foreground color to black...
    sta $d800,x                 // ...in top part of screen
    lda #WHITE                  // set foreground color to white...
    sta $d800+$100,x            // ...in middle part of screen
    lda #BLACK                  // set foreground color to black...
    sta $d800+$200,x            // ...in top part of screen
    sta $d800+$300,x            //
    inx                         //
    bne InitScreen              // loop 256 times

//  display static black text in the top and botton part of the screen
    ldx #SCROLL_TEXT.size()-1
!loop:
    lda Text,x                  // next character
    sta $0400+14+4*40,x         // to top part of screen
    sta $0400+14+20*40,x        // to bottom part of screen
    dex
    bpl !loop-                  // loop through each character

//  init sprite (revolving C= logo)
    lda #Sprite/$40             // set sprite 0 pointer to our sprite graphics
    sta $07f8
    lda #WHITE                  // set sprite 0 color to white
    sta $d027

    lda #0                      // set sprite 0 pos to be...
    sta $d000                   // ...off screen (x)
    sta $d001                   // ...off screen (y)

    lda #%00000000              // disable sprite multicolor
    sta $d01c
    lda #%00000001              // enable sprite 0
    sta $d015

//  init IRQ handlers
    ldy #$7f                    // $7f = %01111111
    sty $dc0d                   // turn off CIAs Timer interrupts
    sty $dd0d                   // turn off CIAs Timer interrupts
    lda $dc0d                   // cancel all CIA-IRQs in queue/unprocessed
    lda $dd0d                   // cancel all CIA-IRQs in queue/unprocessed
    lda #$01                    // set IRQ mask...
    sta $d01a                   // ...to trigger by raster line
    SetNextInterrupt(IRQ0_LINE, Handle0)
    cli                         // clear interrupt disable flag

    jmp *                       // infinite program loop

//--------------------------------------------------------------------------------------------------
//  Interrupt handlers
//--------------------------------------------------------------------------------------------------

//  Handle IRQ0 at raster line IRQ0_LINE
Handle0:
    StoreState()
    SetScreenColor(WHITE)       // set white background for top part of screen
    ResetCharScrollX()          // reset char scroll x to 0 for top static text
    dec frameCount              // iterate through animation frames...
    bpl !skip+
    lda #SCROLL_LENGTH-1        // or reset to 0 if looped through the whole animation sequence
    sta frameCount
!skip:
    SetNextInterrupt(IRQ1_LINE, Handle1)
    RestoreState()

//  Handle IRQ1 at raster line IRQ1_LINE 
Handle1:
    StoreState()
    ldy frameCount                      // load current animation frame
    lda SpriteXPos, y                   // load and...
    sta $d000                           // ...set corresponding sprite x pos
    lda SpriteYPos, y                   // load and...
    sta $d001                           // ...set corresponding sprite y pos
    lda TextShiftPos,y                  // load and...
    sta temp                            // 
    SetCharScrollX(temp)                // ...set corresponding char scroll x pos

    lda TextCharPos,y                   // load position of text for current animation frame...
    tay                                 // ...to use for indexing sreen RAM
    ldx #SCROLL_TEXT.size()-1           // iterate through each character of scrolling text
!loop:
    lda Text,x                          // load next char of scrolling text
    sta $0400+SCROLL_CHAR_ROW_POS*40,y  // and store it in relevant screen RAM position
    dey
    dex
    bpl !loop-                          // loop through each character of the scrolling text
    SetNextInterrupt(IRQ2_LINE, Handle2)
    RestoreState()

//  Handle IRQ2 at raster line IRQ2_LINE
Handle2:
    StoreState()
    nop                         // lazy raster line stabilization
    nop
    nop
    SetScreenColor(BLACK)       // set black background for middle part of screen
    SetNextInterrupt(IRQ3_LINE, Handle3)
    RestoreState()

//  Handle IRQ3 at raster line IRQ3_LINE
Handle3:
    StoreState()
    nop                         // lazy raster line stabilization
    nop
    nop
    SetScreenColor(WHITE)       // set white background for bottom part of screen
    ResetCharScrollX()          // reset char scroll x to 0 for bottom static text
    SetNextInterrupt(IRQ0_LINE, Handle0)
    RestoreState()

//--------------------------------------------------------------------------------------------------
//  Animated text and sprite data
//--------------------------------------------------------------------------------------------------

Text:
//  text to scroll
    .text SCROLL_TEXT
TextCharPos:
//  generate lookup table for char pos of scrolling text for each animation frame
    .for(var i=0;i<SCROLL_LENGTH;i++) .byte floor(textXPos(i)/8)-round(SCROLL_TEXT.size()/2)+SCROLL_TEXT.size()-1
TextShiftPos:
//  generate lookup table for char scroll x pos of scrolling text for each animation frame
    .for(var i=0;i<SCROLL_LENGTH;i++) .byte mod(textXPos(i),8)

//  Debug info: scrolling text positions for each animation frame
    .for(var i=0;i<SCROLL_LENGTH;i++) .print (floor(textXPos(i)/8)*8).string() + " + " + mod(textXPos(i),8) + " = " + textXPos(i)

SpriteXPos:
//  generate lookup table for sprite x pos for each animation frame
    .for(var i=0;i<SCROLL_LENGTH;i++) .byte circleXPos(i)
SpriteYPos:
//  generate lookup table for sprite y pos for each animation frame
    .for(var i=0;i<SCROLL_LENGTH;i++) .byte circleYPos(i)

//--------------------------------------------------------------------------------------------------
//  Sprite graphics (C= logo)
//--------------------------------------------------------------------------------------------------

*=$3200 "Sprites"
Sprite:
    .byte $00,$f8,$00,$03,$fe,$00,$0f,$fe
    .byte $00,$1f,$fe,$00,$3f,$fe,$00,$3f
    .byte $0e,$00,$7c,$01,$fc,$7c,$01,$f8
    .byte $f8,$01,$f0,$f8,$01,$e0,$f0,$00
    .byte $00,$f8,$01,$e0,$f8,$01,$f0,$7c
    .byte $01,$f8,$7c,$01,$fc,$3f,$0e,$00
    .byte $3f,$fe,$00,$1f,$fe,$00,$0f,$fe
    .byte $00,$03,$fe,$00,$00,$f8,$00,$03

//--------------------------------------------------------------------------------------------------
//  Helper functions to calculate scrolling text position
//--------------------------------------------------------------------------------------------------

.function textXPos(i) {
    .return round(160+40*cos(toRadians(360*i/SCROLL_LENGTH)))
}

.function circleXPos(i) {
    .return -round(20*cos(toRadians(360*i/SCROLL_LENGTH)))+172
}

.function circleYPos(i) {
    .return round(22*sin(toRadians(360*i/SCROLL_LENGTH)))+139
}

//--------------------------------------------------------------------------------------------------
//  Macros
//--------------------------------------------------------------------------------------------------

//  SetScreenColor(color)
//  set both border and background to same color
.macro SetScreenColor(color) {
    lda #color
    sta $d020
    sta $d021
}

//  ResetCharScrollX(ptr_to_x)
//  sets horizontal raster scroll to value at ptr_to_x address
.macro SetCharScrollX(ptr_to_x) {
    lda $d016
    and %11111000
    ora ptr_to_x
    ora #%00001000
    sta $d016
}

//  ResetCharScrollX()
//  sets horizontal raster scroll to 0
.macro ResetCharScrollX() {
    lda $d016
    and #%11111000
    sta $d016
}

//  SetNextInterrupt(rasterLine, irqHandler)
//  trigger next interrupt handler (irqHandler) at raster line (rasterLine)
//  for simplicity no support for raster lines > 255
.macro SetNextInterrupt(rasterLine, irqHandler) {
    lda #rasterLine             // trigger next interrupt at #rasterLine
    sta $d012                   // raster line to generate next interrupt

    lda $d011                   // set bit #0 to 0 as we don't need to support
    and #$7f                    // positions > 255
    sta $d011                   //

    lda #<irqHandler            // point IRQ vector to our custom irq routine...
    sta $0314                   // ...lower nibble
    lda #>irqHandler            //
    sta $0315                   // ...upper nibble
}

//  StoreState()
//  push a, x, y registers onto stack
.macro StoreState() {
    pha
    txa 
    pha 
    tya 
    pha
}

//  RestoreState()
//  pull y, x, a registers from stack
.macro RestoreState() {
    asl $d019                   // acknowledge IRQ
    pla
    tay
    pla
    tax 
    pla
    jmp $ea81                   // return to kernel interrupt routine
}
