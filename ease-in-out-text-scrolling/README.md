# Ease-in-out text scrolling

![Screenshot](screenshot.png)

A simple project to demonstrate some basic concepts of Commodore 64 programming in assembly (Kick Assembler):
- Raster interrupts
- Scrolling text (left-right ease-in-out)
- Sprite animation (move along a circular path)
- Basic maths in Kick Assembler
- Functions and macros
- Look-up tables for animation
